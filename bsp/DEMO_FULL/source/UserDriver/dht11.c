#include "api/api.h"
#include "stm32f10x.h"

#define GPIOPIN GPIO_Pin_1
#define GPIOPORT GPIOC

#define DHT11_PIN_H  GPIO_SetBits(GPIOPORT, GPIOPIN)
#define DHT11_PIN_L  GPIO_ResetBits(GPIOPORT, GPIOPIN)

#define Read_DHT11 GPIO_ReadInputDataBit(GPIOPORT, GPIOPIN)

static u8 tdata[4]={0x00,0x00,0x00,0x00};
static u8 sbuf,check;

static void DHT11_PortIN(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIOPIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IN_FLOATING;         
	GPIO_Init(GPIOPORT,&GPIO_InitStructure);
}
static void DHT11_PortOUT(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIOPIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; ;         
	GPIO_Init(GPIOPORT,&GPIO_InitStructure);   
}
static unsigned char StartDHT11(void)
{
	//unsigned char flag;

	DHT11_PortOUT();
	DHT11_PIN_L;
	ApiSleep(2);  //触发开始,总线拉低要大于18ms

	DHT11_PIN_H;
	ApiDelayUS(25);  //wait 20-40uS 等待DHT11的低电平响应信号


	DHT11_PortIN();//改为输入
	if(!Read_DHT11)
	{
		while(!Read_DHT11);//低电平的响应信号，80us
		while(Read_DHT11);//紧接着是80us的高电平数据准备信号
		//拉低后DHT11会拉高总线80us,接着会开始传数据	
		return 1;
	}
	return 0;
}
static void com(void)
{
	u8 i,tt;
	tt = 0;
	for(i = 0;i<8;i++)
	{
		sbuf <<= 1;
		ApiDelayUS(20);//接收到响应后会出现50us的低电平表示发送数据的开始，所以这里小延时一下
		while(!Read_DHT11);//等到高电平的出现，高电平的时间表示的是数据位的0和1
		ApiDelayUS(25);//数据为0的信号时间为26-28us，1则为70us，这里超时检测
		tt = 100;
		while(tt++);
		if(Read_DHT11)//如果还为高
		{
			sbuf |= 0x01;
			ApiDelayUS(30);//这里的延时足够了，40+10+30>70了
		}
		else //如果变低
		{
			sbuf &= 0xfe;
		}
	}
}
u8 ReadDHT11(u16 *humidity , u8 *temperature)
{
	u8 sum;
	u8 *tmp;
	

	if(StartDHT11())	//判断是否已经响应
	{
		com();
		tdata[0]=sbuf;
		com();
		tdata[1]=sbuf;
		com();
		tdata[2]=sbuf;		
		com();
		tdata[3]=sbuf;
		com();
		check = sbuf;
		sum = (tdata[0]+tdata[1]+tdata[2]+tdata[3]);
		
		tmp = (u8*)humidity;
		tmp[0] = tdata[0];
		tmp[1] = tdata[1];
		
		*temperature = tdata[2];
	}
	if(check == sum)
		return(1);
	else	
		return 0;
}
